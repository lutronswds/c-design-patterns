/*
 * main.c
 *
 *  Created on: Aug 17, 2014
 *      Author: jlenz
 */

#include <stdlib.h>
#include "globalStates.h"

/**
 * @brief Program entry point 
 * @param argc The number of command line arguments passed to the program.
 * @param argv The C-string values of each arguement.
 */
int main(int argc, char** argv)
{
   UserInterfaceInit(&StartupState);
   
   /* Pass an event startup doesn't care about. Note how the state object 
    * doesn't react to the event. */
   UserInterfaceQueueEvent(UI_EVENT_BUTTON_PRESS);
   UserInterfaceTask();
   
   /* Cause the startup state to create a state change. */
   UserInterfaceQueueEvent(UI_EVENT_POWER_SUPPLY_GOOD);
   UserInterfaceTask();
   
   /* Note how the button press event is now handled differently based on the 
    * new state. */
   UserInterfaceQueueEvent(UI_EVENT_BUTTON_PRESS);
   UserInterfaceTask();
   
   return EXIT_SUCCESS;
}
