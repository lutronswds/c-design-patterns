/*
 * globalStates.h
 *
 *  Created on: Aug 17, 2014
 *      Author: jlenz
 */

#ifndef GLOBAL_STATES_H_
#define GLOBAL_STATES_H_

#include "userInterface.h"

extern const UI_STATE_BASE StartupState;
extern const UI_STATE_BASE NormalState;

#endif /* GLOBAL_STATES_H_ */
