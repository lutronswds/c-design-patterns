/*
 * userInterface.c
 *
 *  Created on: Aug 17, 2014
 *      Author: lenzj2
 */

#include <string.h>
#include <stdbool.h>
#include "userInterface.h"

/**
 * @brief The number of events the UI's event queue can hold.
 */
#define UI_MAX_NUM_QUEUED_EVENTS (5)

/**
 * @brief Pointer to the currently active state object.
 */
static UI_STATE_BASE const * currentState; 

/**
 * @brief Structure encapsulating data for a queue data structure.
 */
static struct Queue
{
   UI_CONTEXT_EVENT queue[UI_MAX_NUM_QUEUED_EVENTS]; //!< The queue buffer.
   int headIndex; //!< Index of the next entry to be dequeued.
   int tailIndex; //!< Index of the next entry to be enqueued.
   int count; //!< Number of entries in the buffer that are currently valid.
} eventQueue;

static void queueInit(struct Queue * q);
static void queueEnqueue(struct Queue * q, UI_CONTEXT_EVENT event);
static UI_CONTEXT_EVENT queueDequeue(struct Queue * q);

/**
 * @brief Initializes the user interface context module.
 * @param InitialState Pointer to the first active state object.
 */
void UserInterfaceInit(UI_STATE_BASE const * InitialState)
{
   queueInit(&eventQueue);
   UserInterfaceChangeState(InitialState);
}

/**
 * @brief Performs one iteration of the user interface task.
 */
void UserInterfaceTask(void)
{
   currentState->HandleEvent(queueDequeue(&eventQueue));
}

/**
 * @brief Adds an event to the user interface queue for processing by the active state object.
 * @param event The new event to be queued for processing.
 */
void UserInterfaceQueueEvent(UI_CONTEXT_EVENT event)
{
   queueEnqueue(&eventQueue, event);
}

/**
 * @brief Called by a UI state object to change the active state to a new one.
 * @param newState Pointer to the state that should be come active.
 */
void UserInterfaceChangeState(UI_STATE_BASE const * newState)
{
   currentState = newState;
}

/**
 * @brief Initializes a queue object.
 * @param q Pointer to the queue to be initialized.
 */
static void queueInit(struct Queue * q)
{
   q->headIndex = 0;
   q->tailIndex = UI_MAX_NUM_QUEUED_EVENTS - 1;
   q->count = 0;
}

/**
 * @brief Adds an event to the provided queue.
 * @param q Pointer to the queue.
 * @param event The new event to be added.
 */
static void queueEnqueue(struct Queue * q, UI_CONTEXT_EVENT event)
{
   if (q->count < UI_MAX_NUM_QUEUED_EVENTS)
   {
      q->tailIndex = (q->tailIndex + 1) % UI_MAX_NUM_QUEUED_EVENTS;
      q->queue[q->tailIndex] = event;
      q->count = q->count + 1;
   }
}

/**
 * @brief Removes an event to the provided queue.
 * @param q Pointer to the queue.
 * @return The next event for processing. 
 * @return @c UI_EVENT_NULL if the queue is empty.
 */
static UI_CONTEXT_EVENT queueDequeue(struct Queue * q)
{
   UI_CONTEXT_EVENT ret = UI_EVENT_NULL;
   
   if (q->count != 0)
   {
      ret = q->queue[q->headIndex];
      q->headIndex = (q->headIndex + 1) % UI_MAX_NUM_QUEUED_EVENTS;
      q->count = q->count - 1;
   }
   
   return ret;
}
