/*
 * StartupState.c
 *
 *  Created on: Aug 17, 2014
 *      Author: jlenz
 */

#include <stdio.h>
#include "globalStates.h"

static void handleEvent(UI_CONTEXT_EVENT event);

/**
 * @brief Structure encapsulating UI startup state handling.
 */
const UI_STATE_BASE StartupState =
{
   handleEvent
};

/**
 * @brief UI context handler when program is in the startup state.
 * @param event Latest event from UI for processing.
 */
static void handleEvent(UI_CONTEXT_EVENT event)
{
   if (event == UI_EVENT_NULL)
   {
      printf("Startup state handler called with null event.\n");
   }
   else if (event == UI_EVENT_POWER_SUPPLY_GOOD)
   {
      printf("Power supply okay. Transitioning to normal operation.\n");
      UserInterfaceChangeState(&NormalState);
   }
   else
   {
      printf("Startup state handler called with unknown/unhandled event.\n");
   }
}

