/*
 * userInterface.h
 *
 *  Created on: Aug 17, 2014
 *      Author: lenzj2
 */

#ifndef USERINTERFACE_H_
#define USERINTERFACE_H_

typedef enum
{
   UI_EVENT_NULL = 0,
   UI_EVENT_POWER_SUPPLY_GOOD,
   UI_EVENT_BUTTON_PRESS,
   UI_EVENT_BUTTON_RELEASE
} UI_CONTEXT_EVENT;

typedef struct
{
   void (* HandleEvent)(UI_CONTEXT_EVENT);
} UI_STATE_BASE;

void UserInterfaceInit(UI_STATE_BASE const * InitialState);
void UserInterfaceTask(void);
void UserInterfaceQueueEvent(UI_CONTEXT_EVENT event);
void UserInterfaceChangeState(UI_STATE_BASE const * NewState);

#endif /* USERINTERFACE_H_ */
