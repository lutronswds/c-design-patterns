/*
 * normalState.c
 *
 *  Created on: Aug 17, 2014
 *      Author: jlenz
 */

#include <stdio.h>
#include "globalStates.h"

static void handleEvent(UI_CONTEXT_EVENT event);

/**
 * @brief Structure encapsulating UI normal state handling.
 */
const UI_STATE_BASE NormalState =
{
   handleEvent
};

/**
 * @brief UI context handler when program is in normal state.
 * @param event Latest event from UI for processing.
 */
static void handleEvent(UI_CONTEXT_EVENT event)
{
   if (event == UI_EVENT_NULL)
   {
      // Quietly ignore.
   }
   else if (event == UI_EVENT_BUTTON_PRESS)
   {
      printf("Button press handled by normal mode handler.\n");
   }
   else if (event == UI_EVENT_BUTTON_RELEASE)
   {
      printf("Button release handled by normal mode handler.\n");
   }
   else
   {
      printf("Normal state handler called with unknown/unhandled event.\n");
   }
}
