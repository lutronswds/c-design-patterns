/*
 * deviceStateCache.c
 *
 *  Created on: Aug 21, 2014
 *      Author: Jonathan Lenz
 */

#include <stddef.h>
#include <stdlib.h> 
#include "deviceStateCache.h"

/**
 * @brief Structure of a notifiee linked list node.
 */
typedef struct NotifieeListNode
{
   DeviceStateNotifiee notificationFunction; //!< The notification function
   struct NotifieeListNode * next; //!< Pointer to the next list node.
   struct NotifieeListNode * prev; //!< Pointer to the previous list node.
} NOTIFIEE_LIST_NODE;

/**
 * @brief Structure of a notifee linked list.
 */
typedef struct
{
   NOTIFIEE_LIST_NODE * head; //!< Pointer to the list head node.
} NOTIFIEE_LIST;

/**
 * @brief Statically allocated notifiee list structure.
 */
static NOTIFIEE_LIST notifieeList;

/**
 * @brief Latest event reported by each of the devices.
 */
static SYSTEM_EVENT_TYPE lastEvent[MAX_NUM_SYSTEM_DEVICES];

static void notifyStateChange(unsigned device);

/**
 * @brief Initializes the device state cache module.
 */
void DeviceCacheInit(void)
{
   unsigned i;
   notifieeList.head = NULL;
   for (i = 0; i < MAX_NUM_SYSTEM_DEVICES; i++)
   {
      lastEvent[i] = EVENT_NULL;
   }
}

/**
 * @brief Adds a specified notification handler to be called when a device reports an event. 
 * @param notifiee Notification function pointer.
 */
void DeviceCacheAddNotifiee(DeviceStateNotifiee notifiee)
{
   if (notifiee != NULL)
   {
      /* Note: We use dynamic memory to allow a large number of notification 
       * functions to be added for the sake of this example. Obviously other 
       * implementations, such as a statically allocated array of some maximum 
       * number of notifiees, can prevent the need to use dynamic memory. */
      NOTIFIEE_LIST_NODE * node = malloc(sizeof(NOTIFIEE_LIST_NODE));
      if (node != NULL)
      {
         node->prev = NULL;
         node->next = notifieeList.head;
         node->notificationFunction = notifiee;
         
         notifieeList.head = node;
      }
   }
}

/**
 * @brief Removes a specified notification handler from being called.
 * @param notifiee Notification function pointer.
 */
void DeviceCacheRemoveNotifiee(DeviceStateNotifiee notifiee)
{
   NOTIFIEE_LIST_NODE * iter = notifieeList.head;
   while (iter != NULL)
   {
      if (iter->notificationFunction == notifiee)
      {
         ((NOTIFIEE_LIST_NODE *) (iter->next))->prev = iter->prev;
         ((NOTIFIEE_LIST_NODE *) (iter->prev))->next = iter->next;
         
         free(iter);
         
         break;
      }
      
      iter = (NOTIFIEE_LIST_NODE *) (iter->next);
   }
}

/**
 * @brief Accessor for getting the last event reported by a specified device.
 * @param deviceAddress The address of the device.
 * @return The last event reported by the device. 
 * @return @ref EVENT_NONE if no events have been heard by this device.
 */
SYSTEM_EVENT_TYPE DeviceCacheGetLastEvent(unsigned deviceAddress)
{
   if (deviceAddress < MAX_NUM_SYSTEM_DEVICES)
   {
      return lastEvent[deviceAddress];
   }
   else
   {
      return EVENT_NULL;
   }
}

/**
 * @brief Network "event handler" that caches events for all devices.
 * @param deviceAddress The address of the device.
 * @param reportedEvent The event reported by the device.
 */
void DeviceCacheNetworkEventHandler(unsigned deviceAddress,
   SYSTEM_EVENT_TYPE reportedEvent)
{
   if ((deviceAddress < MAX_NUM_SYSTEM_DEVICES)
      && (lastEvent[deviceAddress] != reportedEvent))
   {
      lastEvent[deviceAddress] = reportedEvent;
      notifyStateChange(deviceAddress);
   }
}

/**
 * @brief Notifies all registered notifiees that a device has changed state.
 * @param device The address of the device that has changed state.
 */
static void notifyStateChange(unsigned device)
{
   NOTIFIEE_LIST_NODE * iter = notifieeList.head;
   while (iter != NULL)
   {
      iter->notificationFunction(device);
      iter = (NOTIFIEE_LIST_NODE *) (iter->next);
   }
}
