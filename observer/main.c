/*
 * main.c
 *
 *  Created on: Aug 17, 2014
 *      Author: Jonathan Lenz
 */

#include <stdlib.h>

#include "consoleLogger.h"
#include "deviceStateCache.h"
#include "integrationReporter.h"

/**
 * @brief Observer program entry point.
 * @param argc The number of command line arguments which which program was called.
 * @param argv Array of strings of command line arguments.
 * @return @c EXIT_SUCCESS at end of program execution.
 */
int main(int argc, char ** argv)
{
   DeviceCacheInit();
   ConsoleLoggerInit();
   IntegrationInit();
   
   /* Notice how our logger gets called with event data. */
   DeviceCacheNetworkEventHandler(7, BUTTON_PRESS);
   DeviceCacheNetworkEventHandler(7, BUTTON_RELEASE);
   
   /* Suppose user now enables integration by some means. */
   IntegrationSetState(INTEGRATION_ON);
   
   /* Now see how integration events are now sent to the console. */
   DeviceCacheNetworkEventHandler(7, BUTTON_PRESS);
   DeviceCacheNetworkEventHandler(7, BUTTON_RELEASE);
   
   return EXIT_SUCCESS;
}
