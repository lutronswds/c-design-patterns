/*
 * consoleLogger.h
 *
 *  Created on: Aug 21, 2014
 *      Author: Jonathan Lenz
 */

#ifndef CONSOLELOGGER_H_
#define CONSOLELOGGER_H_

void ConsoleLoggerInit(void);

#endif /* CONSOLELOGGER_H_ */
