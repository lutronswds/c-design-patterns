/*
 * integrationReporter.c
 *
 *  Created on: Aug 24, 2014
 *      Author: Jonathan Lenz
 */

#include <stdio.h>
#include "deviceStateCache.h"
#include "integrationReporter.h"

/**
 * @brief The current user set state of integration reporting.  
 */
static INTEGRATION_SETTING currentIntegrationReportLevel;

static void deviceStateReactor(unsigned device);

/**
 * @brief Initializes the integration reporting module.
 */
void IntegrationInit(void)
{
   currentIntegrationReportLevel = INTEGRATION_OFF;
}

/**
 * @brief Sets the state of integration reporting.
 * @param integrationSetting The new integration state.
 */
void IntegrationSetState(INTEGRATION_SETTING integrationSetting)
{
   if (integrationSetting != currentIntegrationReportLevel)
   {
      if (integrationSetting == INTEGRATION_ON)
      {
         DeviceCacheAddNotifiee(deviceStateReactor);
      }
      else
      {
         DeviceCacheRemoveNotifiee(deviceStateReactor);
      }
      currentIntegrationReportLevel = integrationSetting;
   }
}

/**
 * @brief Device state cache notification handler.
 * 
 * This function reacts to changes in the device state cache for the purpose 
 * of generating integration events to the console.
 * @param device The number of the device that has changed state.
 */
static void deviceStateReactor(unsigned device)
{
   SYSTEM_EVENT_TYPE reportedEvent = DeviceCacheGetLastEvent(device);
   
   if (reportedEvent == BUTTON_PRESS)
   {
      printf("PRESS,%d\n", device);
   }
   else if (reportedEvent == BUTTON_RELEASE)
   {
      printf("RELEASE,%d\n", device);
   }
}


