/*
 * consoleLogger.c
 *
 *  Created on: Aug 21, 2014
 *      Author: Jonathan Lenz
 */

#include <stdio.h>
#include "deviceStateCache.h"

static void deviceStateReactor(unsigned device);

/**
 * @brief Initializes a logging module that logs to the console.
 */
void ConsoleLoggerInit(void)
{
   DeviceCacheAddNotifiee(deviceStateReactor);
}

/**
 * @brief Device state cache notification handler.
 * 
 * This function reacts to changes in the device state cache and logs them to 
 * the console for debugging purposes.
 * @param device The number of the device that has changed state.
 */
static void deviceStateReactor(unsigned device)
{
   printf("Event: Device %d reported event %d\n", device,
      DeviceCacheGetLastEvent(device));
}


