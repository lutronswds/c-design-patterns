/*
 * deviceStateCache.h
 *
 *  Created on: Aug 21, 2014
 *      Author: Jonathan Lenz
 */

#ifndef DEVICESTATECACHE_H_
#define DEVICESTATECACHE_H_

#define MAX_NUM_SYSTEM_DEVICES (50)

typedef enum SystemEvent
{
   EVENT_NULL = 0,
   BUTTON_PRESS,
   BUTTON_RELEASE
} SYSTEM_EVENT_TYPE;

typedef void (* DeviceStateNotifiee)(unsigned);

void DeviceCacheInit(void);

void DeviceCacheAddNotifiee(DeviceStateNotifiee notifiee);
void DeviceCacheRemoveNotifiee(DeviceStateNotifiee notifiee);

SYSTEM_EVENT_TYPE DeviceCacheGetLastEvent(unsigned deviceAddress);
void DeviceCacheNetworkEventHandler(unsigned deviceAddress, SYSTEM_EVENT_TYPE reportedEvent);

#endif /* DEVICESTATECACHE_H_ */
