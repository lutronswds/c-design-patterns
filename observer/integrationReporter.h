/*
 * integrationReporter.h
 *
 *  Created on: Aug 24, 2014
 *      Author: Jonathan Lenz
 */

#ifndef INTEGRATIONREPORTER_H_
#define INTEGRATIONREPORTER_H_

typedef enum
{
   INTEGRATION_OFF = 0,
   INTEGRATION_ON
} INTEGRATION_SETTING;

void IntegrationInit(void);
void IntegrationSetState(INTEGRATION_SETTING integrationSetting);

#endif /* INTEGRATIONREPORTER_H_ */
