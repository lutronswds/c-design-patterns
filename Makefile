.PHONY: all state observer singleton iterator clean

all: state observer singleton iterator

state:
	@$(MAKE) -C state all

observer:
	@$(MAKE) -C observer all

singleton:
	@$(MAKE) -C singleton all

iterator:
	@$(MAKE) -C iterator all

clean:
	@$(MAKE) -C state clean
	@$(MAKE) -C observer clean
	@$(MAKE) -C singleton clean
	@$(MAKE) -C iterator clean
