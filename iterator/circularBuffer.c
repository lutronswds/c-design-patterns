/*
 * circularBuffer.c
 *
 *  Created on: Aug 25, 2014
 *      Author: Parker Evans
 */

#include <stdio.h>
#include "myAlloc.h"
#include "circularBuffer.h"

// Private representation of a circular buffer
struct CIRCULAR_BUFFER_INTERNAL
{
   unsigned short capacity;
   unsigned short count;
   void* startPtr;
   void* endPtr;
   void** readPtr;
   void** writePtr;
};

/**
 * @brief Create a new circular buffer (uses internal static free store)
 * @param numElements The capacity of the requested buffer
 * @return The new circular buffer (or null if there was a failure)
 */
CIRCULAR_BUFFER CircularBufferNew(unsigned short numElements)
{
   void* mem;
   CIRCULAR_BUFFER buf;
   unsigned int usedSize;

   usedSize = sizeof(struct CIRCULAR_BUFFER_INTERNAL) + (sizeof(void*) * numElements) + 1;
   mem = MemAlloc(usedSize);
   if (mem)
   {
      buf = (CIRCULAR_BUFFER)mem;
      buf->capacity = numElements;
      buf->count = 0;
      buf->startPtr = (void*)(((char*)mem) + sizeof(struct CIRCULAR_BUFFER_INTERNAL));
      buf->endPtr = (void*)(((char*)mem) + usedSize);
      buf->readPtr = (void**)(buf->startPtr);
      buf->writePtr = (void**)(buf->startPtr);
   }
   else
   {
      buf = (CIRCULAR_BUFFER)((void*)0);
   }

   return buf;
}

/**
 * @brief Get the capacity of a specified buffer.
 * @param buf The circular buffer
 * @return The capacity of the specified buffer as unsigned short
 */
unsigned short CircularBufferCapacity(CIRCULAR_BUFFER buf)
{
   return buf->capacity;
}

/**
 * @brief Get the number of items in a specified buffer.
 * @param buf The circular buffer
 * @return The count of elements in the buffer as unsigned short
 */
unsigned short CircularBufferCount(CIRCULAR_BUFFER buf)
{
   return buf->count;
}

/**
 * @brief Push an item onto the circular buffer.
 * @param buf The circular buffer
 * @param element The element to put on the buffer
 * @return A return code indicating success or failure
 */
CIRCULAR_BUFFER_RET_CODE CircularBufferPushBack(CIRCULAR_BUFFER buf, void* element)
{
   void** next;
   CIRCULAR_BUFFER_RET_CODE retCode = CIRCULAR_BUFFER_SUCCESS;

   // Note: since we control these things, we have the invariant that
   // the buffer is sizeof(void*) aligned.  Thus we ignore the possibility
   // that we are going to overflow the buffer.

   // Determine the next location
   next = buf->writePtr + 1;
   if (next == buf->endPtr)
   {
      next = buf->startPtr;
   }

   // Now check if we are full
   if (next == buf->readPtr)
   {
      // We are full!
      retCode = CIRCULAR_BUFFER_FAILURE;
   }
   else
   {
      buf->count++;
      *(buf->writePtr) = element;
      buf->writePtr = next;
   }

   return retCode;
}

/**
 * @brief Pop an item off the circular buffer.
 * @param buf The circular buffer
 * @param element A pointer to store the popped element into
 * @return A return code indicating success or failure
 */
CIRCULAR_BUFFER_RET_CODE CircularBufferPopFront(CIRCULAR_BUFFER buf, void** element)
{
   CIRCULAR_BUFFER_RET_CODE retCode = CIRCULAR_BUFFER_SUCCESS;

   if (buf->readPtr == buf->writePtr)
   {
      // We are empty!
      retCode = CIRCULAR_BUFFER_FAILURE;
   }
   else
   {
      buf->count--;
      *element = *buf->readPtr;
      buf->readPtr++;
      if (buf->readPtr == buf->endPtr)
      {
         buf->readPtr = buf->startPtr;
      }
   }

   return retCode;
}

/**
 * @brief Get the begin iterator of a circular buffer
 * @param buf The circular buffer
 * @return The begin iterator of the circular buffer
 */
CIRCULAR_BUFFER_ITERATOR CircularBufferBegin(CIRCULAR_BUFFER buf)
{
   return buf->readPtr;
}

/**
 * @brief Get the end iterator of a circular buffer
 * @param buf The circular buffer
 * @return The end iterator of the circular buffer
 */
CIRCULAR_BUFFER_ITERATOR CircularBufferEnd(CIRCULAR_BUFFER buf)
{
   return buf->writePtr;
}

/**
 * @brief Get the next item iterator based on the given iterator
 * @param iter The current iterator (stores the new iterator in the current)
 * @param buf The circular buffer
 * @return The end iterator of the circular buffer
 */
CIRCULAR_BUFFER_ITERATOR CircularBufferIterNext(CIRCULAR_BUFFER_ITERATOR iter, CIRCULAR_BUFFER buf)
{
   // Save them from themselves
   if (iter != buf->writePtr)
   {
      iter++;
      if (iter == buf->endPtr)
      {
         iter = buf->startPtr;
      }
   }

   return iter;
}

/**
 * @brief Check if the iterator is done (no more items)
 * @param iter The current iterator (stores the new iterator in the current)
 * @param buf The circular buffer
 * @return Zero if not done, non-zero otherwise
 */
int CircularBufferIterIsDone(CIRCULAR_BUFFER_ITERATOR iter, CIRCULAR_BUFFER buf)
{
   return (iter == buf->writePtr);
}

/**
 * @brief Get the element from the iterator (null if end)
 * @param iter The current iterator
 * @return The element the iterator is pointing to and null if at end
 */
void* CircularBufferIterGetElement(CIRCULAR_BUFFER_ITERATOR iter, CIRCULAR_BUFFER buf)
{
   if (CircularBufferIterIsDone(iter, buf))
   {
      return (void*)0;
   }
   else
   {
      return *iter;
   }
}
