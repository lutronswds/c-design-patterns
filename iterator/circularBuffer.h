/*
 * circularBuffer.h
 *
 *  Created on: Aug 25, 2014
 *      Author: Parker Evans
 */

#ifndef CIRCULARBUFFER_H_
#define CIRCULARBUFFER_H_

// Public data types
typedef struct CIRCULAR_BUFFER_INTERNAL *CIRCULAR_BUFFER;
typedef void** CIRCULAR_BUFFER_ITERATOR;

typedef enum
{
   CIRCULAR_BUFFER_SUCCESS,
   CIRCULAR_BUFFER_FAILURE
} CIRCULAR_BUFFER_RET_CODE;

// Public Circular Buffer API
CIRCULAR_BUFFER CircularBufferNew(unsigned short);
unsigned short CircularBufferCapacity(CIRCULAR_BUFFER);
unsigned short CircularBufferCount(CIRCULAR_BUFFER);
CIRCULAR_BUFFER_RET_CODE CircularBufferPushBack(CIRCULAR_BUFFER, void*);
CIRCULAR_BUFFER_RET_CODE CircularBufferPopFront(CIRCULAR_BUFFER, void**);
CIRCULAR_BUFFER_ITERATOR CircularBufferBegin(CIRCULAR_BUFFER);
CIRCULAR_BUFFER_ITERATOR CircularBufferEnd(CIRCULAR_BUFFER);

// Public Circular Buffer Iterator API
CIRCULAR_BUFFER_ITERATOR CircularBufferIterNext(CIRCULAR_BUFFER_ITERATOR, CIRCULAR_BUFFER);
int CircularBufferIterIsDone(CIRCULAR_BUFFER_ITERATOR, CIRCULAR_BUFFER);
void* CircularBufferIterGetElement(CIRCULAR_BUFFER_ITERATOR, CIRCULAR_BUFFER);

#endif /* CIRCULARBUFFER_H_ */
