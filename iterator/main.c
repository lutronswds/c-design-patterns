/*
 * main.c
 *
 *  Created on: Aug 25, 2014
 *      Author: Parker Evans
 */

#include <stdlib.h>
#include <stdio.h>
#include "circularBuffer.h"

// Some fun values to store in the buffer
static char* HELLO = "HELLO";
static char* SUP = "SUP";
static char* YO = "YO";
static char* HEY = "HEY";

/**
 * @brief Iterator program entry point.
 * @param argc The number of command line arguments which which program was called.
 * @param argv Array of strings of command line arguments.
 * @return @c EXIT_SUCCESS at end of program execution.
 */
int main(int argc, char ** argv)
{
   CIRCULAR_BUFFER_ITERATOR iter;
   CIRCULAR_BUFFER a, b;
   void* tempElement;

   // Setup the buffer and add some stuff
   a = CircularBufferNew(5);
   if (!a)
   {
      printf("Failure to allocate buffer!\n");
      exit(EXIT_FAILURE);
   }
   (void)CircularBufferPushBack(a, HELLO);
   (void)CircularBufferPushBack(a, SUP);
   (void)CircularBufferPushBack(a, YO);
   (void)CircularBufferPushBack(a, HEY);

   b = CircularBufferNew(6);
   if (!b)
   {
      printf("Failure to allocate buffer!\n");
      exit(EXIT_FAILURE);
   }
   (void)CircularBufferPushBack(b, HELLO);
   (void)CircularBufferPushBack(b, SUP);
   (void)CircularBufferPushBack(b, YO);
   (void)CircularBufferPushBack(b, HEY);
   (void)CircularBufferPopFront(b, &tempElement);
   (void)CircularBufferPopFront(b, &tempElement);
   (void)CircularBufferPushBack(b, HELLO);
   (void)CircularBufferPushBack(b, SUP);
   (void)CircularBufferPushBack(b, YO);
   (void)CircularBufferPushBack(b, HEY);

   printf("Buffer a:\n");
   for (iter = CircularBufferBegin(a); !CircularBufferIterIsDone(iter, a);
      iter = CircularBufferIterNext(iter, a))
   {
      // Print the element
      printf("%s\n", (char *)CircularBufferIterGetElement(iter, a));
   }
   printf("\n");

   printf("Buffer b:\n");
   for (iter = CircularBufferBegin(b); !CircularBufferIterIsDone(iter, b);
      iter = CircularBufferIterNext(iter, b))
   {
      // Print the element
      printf("%s\n", (char *)CircularBufferIterGetElement(iter, b));
   }
   printf("\n");
   
   return EXIT_SUCCESS;
}
