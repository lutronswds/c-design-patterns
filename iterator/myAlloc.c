/*
 * myAlloc.c
 *
 *  Created on: Aug 25, 2014
 *      Author: Parker Evans
 */

#include "myAlloc.h"

// Make 2K of one-time allocatable memory
#define ALLOC_MEM_SIZE (2048)

/**
 * @brief Allocate memory from a free store (one-time allocation)
 * @param size Requested size in bytes of the memory allocation
 * @return Pointer to the allocated memory
 * @note This function is only suitable for a one time static allocation
 */
void* MemAlloc(unsigned int size)
{
   static char freeStore[ALLOC_MEM_SIZE];
   static unsigned int freeStoreIndex = 0;

   void* ptr = (void*)0;
   if (size <= (ALLOC_MEM_SIZE - freeStoreIndex))
   {
      ptr = (void*)&(freeStore[freeStoreIndex]);
      freeStoreIndex += size;
   }

   return ptr;
}