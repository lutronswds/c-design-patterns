/*
 * myAlloc.h
 *
 *  Created on: Aug 25, 2014
 *      Author: Parker Evans
 */

#ifndef MYALLOC_H_
#define MYALLOC_H_

// Internal one-time alloc function (limit allocations to 65535)
void* MemAlloc(unsigned int);

#endif /* MYALLOC_H_ */
