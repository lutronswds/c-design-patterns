# README #

Design Patterns can be thought of as templates or algorithms for structuring software to solve some particular architectural (rather than functional) problem.  They are typically taught using object oriented languages such as C++, C#, or Java.  Even at Lutron, it may be tempting to think that such concepts cannot be applied to C programming or embedded software development.  However, these concepts, which promote such ideals as loose coupling, encapsulation, and dependency injection, can be just as useful for software written in C.  While the implementation of many of these concepts is more straight-forward when using dynamic memory allocation (which we will show), quite a few of these concepts can be employed without it and are thus appropriate for use in embedded software development as well.

This repository contains some examples of design patterns implemented in C.

### Contents ###

* State Pattern
* Observer Pattern
* Singleton
* Iterator
