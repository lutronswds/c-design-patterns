ifeq "$(SRC)" ""
$(error No source files specified for target. Cancelling build.)
endif

ifeq "$(TARGET_NAME)" ""
$(error Target name not specified. Cancelling build.)
endif

ifeq "$(OBJ_DIR)" ""
OBJ_DIR = .
endif

OBJ = $(addprefix $(OBJ_DIR)/,$(subst .c,.o,$(SRC)))
DEP = $(addprefix $(OBJ_DIR)/,$(subst .c,.d,$(SRC)))
TARGET = $(OBJ_DIR)/$(TARGET_NAME)

ifneq "$(MAKECMDGOALS)" "clean"
-include $(DEP)
endif

.PHONY: all
all: $(TARGET)

$(TARGET): $(OBJ)
	@mkdir -p $(dir $@)
	$(LINK.o) -g -o $@ $^

$(OBJ_DIR)/%.o: %.c
	@mkdir -p $(dir $@)
	$(COMPILE.c) -g -MMD -MP -o $@ $<

.PHONY: clean
clean:
	@rm -r $(OBJ_DIR)
