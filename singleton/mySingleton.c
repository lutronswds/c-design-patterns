/*
 * mySingleton.c
 *
 *  Created on: Aug 21, 2014
 *      Author: pevans
 */

#include "mySingleton.h"

/**
 * @brief Singleton instance of our object.
 */
static MY_SINGLETON instance = {
   0
};

/**
 * @brief Get the static single instance of our object.
 */
MY_SINGLETON* SingletonGetInstance(void)
{
   return &instance;
}

/**
 * @brief Modify the singleton (usually bad practice).
 */
void SingletonChangeNumber(MY_SINGLETON* s)
{
   /* Update the singleton */
   s->MyNumber = (s->MyNumber*743) + 7;
}

/**
 * @brief Get some property of the singleton.
 */
int SingletonGetNumber(MY_SINGLETON* s)
{
   return s->MyNumber;
}
