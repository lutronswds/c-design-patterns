/*
 * mySingleton.h
 *
 *  Created on: Aug 21, 2014
 *      Author: pevans
 */

#ifndef MYSINGLETON_H_
#define MYSINGLETON_H_

typedef struct
{
   int MyNumber;
} MY_SINGLETON;

MY_SINGLETON* SingletonGetInstance(void);

void SingletonChangeNumber(MY_SINGLETON*);
int SingletonGetNumber(MY_SINGLETON*);

#endif /* MYSINGLETON_H_ */
