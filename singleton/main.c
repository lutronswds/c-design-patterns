/*
 * main.c
 *
 *  Created on: Aug 21, 2014
 *      Author: pevans
 */

#include <stdlib.h>
#include <stdio.h>
#include "mySingleton.h"

/**
 * @brief Program entry point 
 * @param argc The number of command line arguments passed to the program.
 * @param argv The C-string values of each arguement.
 */
int main(int argc, char** argv)
{
   MY_SINGLETON* s = SingletonGetInstance();

   printf("Number before: %d\n", SingletonGetNumber(s));
   SingletonChangeNumber(s);
   printf("Number after: %d\n", SingletonGetNumber(s));
   
   return EXIT_SUCCESS;
}
